unmapAll

map i passNextKey

map j scrollDown
map k scrollUp
map gg scrollToTop
map G scrollToBottom

map f LinkHints.activateMode
map F LinkHints.activateModeToOpenInNewForegroundTab
map t LinkHints.activateModeToOpenInNewTab
map af LinkHints.activateModeWithQueue
map adf LinkHints.activateModeToDownloadLink
map aif LinkHints.activateModeToOpenIncognito
map ayf LinkHints.activateModeToCopyLinkUrl

map yy copyCurrentUrl
map dd removeTab

map v enterVisualMode
