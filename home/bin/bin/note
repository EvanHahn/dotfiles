#!/usr/bin/env python3
import argparse
import os
from pathlib import Path
import subprocess
from datetime import datetime


def get_filename(now, title):
    now_str = now.strftime("%Y-%m-%d")

    title = title.strip().replace("/", "_").replace("\\", "_").replace(":", "-")
    if title == "":
        return f"{now_str}.md"

    return f"{now_str} {title}.md"


def get_editor():
    return os.getenv("VISUAL") or os.getenv("EDITOR") or "vim"


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("title", nargs="?", default="", help="Note title")
    parser.add_argument(
        "-a", "--archive", action="store_true", help="Put note in archive instead"
    )
    args = parser.parse_args()

    now = datetime.now()

    filename = get_filename(now, args.title)

    notes_path = Path.home() / "notes"
    path = notes_path
    if args.archive:
        path /= "archive"
        path /= now.strftime("%Y")
    path.mkdir(mode=0o700, parents=True, exist_ok=True)

    path /= filename

    subprocess.run([get_editor(), path], cwd=notes_path, check=True)


if __name__ == "__main__":
    main()
